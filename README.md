# Online Orders Integration / Broker API

### What is this repository for? ###

* An example of how to authenticate and integrate with SBI's Online Orders
* All example URLs connect to testing databases

#### Broker Authentication ####

In order to use the broker API, a user of the SBI system will need to add invite the broker to the SBI Platform for their company. 

Upon receiving the invite, you will need to create a login. Currently the broker API does NOT support authenticating via Google or Azure.

Once added, you will be able to login and access the broker portion of the Online Order API.

These examples will connect to a testing API. For connecting to a production Online Order Broker API for a specific customer they will need to provide you with their URL.

In all examples, the number following /api/ is the company code. Each company will have a unique code. You should request the base url and code from the company directly.

The following examples all run against a testing company.

* Url: https://apps.sbiteam.com/orders-qa/api/100198/broker/login
  * POST to this url
  * Body should be x-www-form-urlencoded
  * Keys
    * username: (your platform username)
    * password: (your platform password)
	
**Example Request**

```
POST /orders-qa/api/100335/broker/login HTTP/1.1
Content-Type: application/x-www-form-urlencoded
User-Agent: PostmanRuntime/7.16.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 6c5a7bdf-6d6e-4435-b6a1-c7bca250f3e6
Host: apps.sbiteam.com
Accept-Encoding: gzip, deflate
Content-Length: 56
Connection: keep-alive

username=[email]&password=[password]
```

**Example Response**

```
HTTP/1.1 200 OK
Server: nginx/1.10.3 (Ubuntu)
Date: Mon, 23 Sep 2019 16:06:45 GMT
Content-Type: application/json; charset=utf-8
Connection: keep-alive
X-Powered-By: Express
App-Version: 1.5.34
ETag: W/"41b-DyEe5H9Ihw60AaoOPfQTF6NJ4XQ"
Vary: Accept-Encoding
Content-Length: 1051

{"access_token":"[hidden]","expires_in":3600,"token_type":"Bearer","scope":"email online-orders online-orders-api openid"}
```

#### Token Usage ####

* The access_token found in the token response is used to authorize future requests
* The access_token is the request Header as the Authorization

#### View Ship-To and Availability Template Access ####

* Url: /orders-qa/api/100335/broker/customers
* GET to this url
* Header requires Authorization 
* You may receive one, or multiple customer ship-to's that you have access to depending on how your user was set up

**Example Request**

```
GET /orders-qa/api/100335/broker/customers HTTP/1.1
Authorization: Bearer [hidden]
User-Agent: PostmanRuntime/7.16.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 50db6a26-a03d-4424-b685-170dbcdf6396
Host: apps.sbiteam.com
Accept-Encoding: gzip, deflate
Connection: keep-alive
```

**Example Response**

```
HTTP/1.1 200 OK
Server: nginx/1.10.3 (Ubuntu)
Date: Mon, 23 Sep 2019 16:39:43 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 345
Connection: keep-alive
X-Powered-By: Express
App-Version: 1.5.34
ETag: W/"159-R3Qtgvj/0ZF5FOtCLrD5GPfgbUM"
Vary: Accept-Encoding


[{"cntID":13305,"custID":9117,"shipID":"Default","billToCompany":"Company LLC","shipToCompany":"Company LLC","billToAddress":"1234 Road Lane, Metropolis, NY 12345","shipToAddress":"1234 Road Lane, Metropolis, NY 12345","template":7,"blockPO":false,"pricing":"Multi-Level Pricing","secondaryPricing":null,"templates":"2,6,7","defaultTemplate":7}]
```

**Response Field Descriptions**

Field | Description
--- | ---
cntID | This is the Id number for the Ship-To
custID | This is the Id number for the Ship-To's Bill-To (when working with the Broker API you will mainly just need the cntID)
templates | These are the availability template numbers that you have access to. This list can be zero-to-many.


#### View Catalog Availability ####

* Url: https://apps.sbiteam.com/orders-qa/api/100335/broker/catalog?template=2&shipId=13305
* The template and shipId values above should match what was returned by the /broker/customers endpoint
* GET to this url
* Header requires Authorization 


**Example Request**

```
GET https://apps.sbiteam.com/orders-qa/api/100335/broker/catalog HTTP/1.1
Authorization: Bearer [HIDDEN]
User-Agent: PostmanRuntime/7.13.0
Accept: */*
Cache-Control: no-cache
Postman-Token: 12bb993d-ab40-47fd-9989-9e12037c32b2
Host: apps.sbiteam.com
accept-encoding: gzip, deflate
Connection: keep-alive
```

**Example Response**

```
HTTP/1.1 200 OK
Server: nginx/1.10.3 (Ubuntu)
Date: Thu, 06 Jun 2019 19:16:31 GMT
Content-Type: application/json; charset=utf-8
Connection: keep-alive
X-Powered-By: Express
App-Version: 1.5.32
ETag: W/"c0676-D/GQysCSdujXhey/PUwanlcXdic"
Vary: Accept-Encoding
Content-Length: 788086

[{"pk":22,"prodId":"AMSONXXXBLUE32","itemKey":20,"size":"32","sizeSort":0,"description":"Amsonia 'Blue Ice'","catalog":"This compact Blue Star is covered in powder blue flowers for several weeks in early summer. Its foliage turns brilliant yellow in fall. Blue Ice spreads slowly to form a tidy groundcover. Easy to grow and drought tolerant once established.","flowerColor":"Lavender Blue","foliageColor":"","flowerTime":"Early Spring","height":"12-15\"","spread":"24\"","type":"Default","exposure":"Full Sun to Part Sun","commonName":"Bluestar","category":"Perennial","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"Average","growth":"Medium","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"UR","cf2":"","cf3":"","templateId":1,"availQty":366,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":549,"prodId":"AMSONXXXBLUE32","itemKey":20,"size":"32","sizeSort":0,"description":"Amsonia 'Blue Ice'","catalog":"This compact Blue Star is covered in powder blue flowers for several weeks in early summer. Its foliage turns brilliant yellow in fall. Blue Ice spreads slowly to form a tidy groundcover. Easy to grow and drought tolerant once established.","flowerColor":"Lavender Blue","foliageColor":"","flowerTime":"Early Spring","height":"12-15\"","spread":"24\"","type":"Default","exposure":"Full Sun to Part Sun","commonName":"Bluestar","category":"Perennial","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"Average","growth":"Medium","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"UR","cf2":"","cf3":"","templateId":2,"availQty":366,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":563,"prodId":"ASTERTATJIND32","itemKey":55,"size":"32","sizeSort":0,"description":"Aster tataricus 'Jindai'","catalog":"Bold textured foliage provides a wonderful background for summer perennials. In Fall it is topped with surprisingly delicate flowers with soft pink petals and bright yellow centers. Irresistible to butterflies! Long-lived and easy to grow.","flowerColor":"Light Purple","foliageColor":"","flowerTime":"Fall","height":"3-4'","spread":"2-3'","type":"Default","exposure":"Full Sun","commonName":"Tatarian daisy","category":"Perennial","comment1":"","zone":"4-8","fallColor":"","shapeForm":"","soil":"Average to Moist","growth":"Medium","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"UR","cf2":"","cf3":"","templateId":2,"availQty":75,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":45,"prodId":"ASTERTATJIND32","itemKey":55,"size":"32","sizeSort":0,"description":"Aster tataricus 'Jindai'","catalog":"Bold textured foliage provides a wonderful background for summer perennials. In Fall it is topped with surprisingly delicate flowers with soft pink petals and bright yellow centers. Irresistible to butterflies! Long-lived and easy to grow.","flowerColor":"Light Purple","foliageColor":"","flowerTime":"Fall","height":"3-4'","spread":"2-3'","type":"Default","exposure":"Full Sun","commonName":"Tatarian daisy","category":"Perennial","comment1":"","zone":"4-8","fallColor":"","shapeForm":"","soil":"Average to Moist","growth":"Medium","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"UR","cf2":"","cf3":"","templateId":1,"availQty":75,"qtyShip":1,"comments":"","price":0,"grade":"","triumphId": 152101,"imgs":[]},{"pk":72,"prodId":"ATHYRNIPPICT32","itemKey":73,"size":"32","sizeSort":0,"description":"Athyrium niponicum 'Pictum'","catalog":"The most colorful fern around with subtle shades of green, purple and red on a grey-blue background. The color is more intense with some direct sun, preferably morning or late afternoon. Strong-growing and dependable, the Lady Ferns are great garden plant","flowerColor":"","foliageColor":"Gray-Blue w/ purple & red accents","flowerTime":"","height":"12-18\"","spread":"18-24\"","type":"Default","exposure":"Shade to Part Shade","commonName":"Japanese painted fern","category":"Fern","comment1":"","zone":"3-8","fallColor":"","shapeForm":"","soil":"Moist","growth":"Medium","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"PG","cf2":"","cf3":"","templateId":1,"availQty":1253,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":577,"prodId":"ATHYRNIPPICT32","itemKey":73,"size":"32","sizeSort":0,"description":"Athyrium niponicum 'Pictum'","catalog":"The most colorful fern around with subtle shades of green, purple and red on a grey-blue background. The color is more intense with some direct sun, preferably morning or late afternoon. Strong-growing and dependable, the Lady Ferns are great garden plant","flowerColor":"","foliageColor":"Gray-Blue w/ purple & red accents","flowerTime":"","height":"12-18\"","spread":"18-24\"","type":"Default","exposure":"Shade to Part Shade","commonName":"Japanese painted fern","category":"Fern","comment1":"","zone":"3-8","fallColor":"","shapeForm":"","soil":"Moist","growth":"Medium","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"PG","cf2":"","cf3":"","templateId":2,"availQty":1253,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":576,"prodId":"ATHYRNIPREGA32","itemKey":72,"size":"32","sizeSort":0,"description":"Athyrium niponicum 'Regal Red'","catalog":"Handsome and ruffled, this high-color selection has been a much requested Japanese Painted Fern. The dark violet red interior of each 'Regal Red' frond is contrasted by bright silver edges making each leaflet distinct and creating an overall tapestry effe","flowerColor":"","foliageColor":"Gray-Blue w/ violet red interior","flowerTime":"","height":"12-18\"","spread":"18-24\"","type":"Default","exposure":"Shade to Part Shade","commonName":"Japanese painted fern","category":"Fern","comment1":"","zone":"3-8","fallColor":"","shapeForm":"","soil":"Moist","growth":"Medium","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"PG","cf2":"","cf3":"","templateId":2,"availQty":632,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":71,"prodId":"ATHYRNIPREGA32","itemKey":72,"size":"32","sizeSort":0,"description":"Athyrium niponicum 'Regal Red'","catalog":"Handsome and ruffled, this high-color selection has been a much requested Japanese Painted Fern. The dark violet red interior of each 'Regal Red' frond is contrasted by bright silver edges making each leaflet distinct and creating an overall tapestry effe","flowerColor":"","foliageColor":"Gray-Blue w/ violet red interior","flowerTime":"","height":"12-18\"","spread":"18-24\"","type":"Default","exposure":"Shade to Part Shade","commonName":"Japanese painted fern","category":"Fern","comment1":"","zone":"3-8","fallColor":"","shapeForm":"","soil":"Moist","growth":"Medium","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"PG","cf2":"","cf3":"","templateId":1,"availQty":633,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":35,"prodId":"1236814","itemKey":1567,"size":"32","sizeSort":0,"description":"Athyrium otophorum NEW","catalog":"clumping, zones 5-9, bright light green color; deciduous but may remain evergreen in milder climates","flowerColor":"","foliageColor":"Bright green","flowerTime":"","height":"16-24\"","spread":"","type":"Default","exposure":"Part Shade to Full Shade","commonName":"","category":"Fern","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"PG","cf2":"","cf3":"","templateId":1,"availQty":75,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":774,"prodId":"1236814","itemKey":1567,"size":"32","sizeSort":0,"description":"Athyrium otophorum NEW","catalog":"clumping, zones 5-9, bright light green color; deciduous but may remain evergreen in milder climates","flowerColor":"","foliageColor":"Bright green","flowerTime":"","height":"16-24\"","spread":"","type":"Default","exposure":"Part Shade to Full Shade","commonName":"","category":"Fern","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"PG","cf2":"","cf3":"","templateId":2,"availQty":75,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":68,"prodId":"ATHYRXXXGHOS32","itemKey":69,"size":"32","sizeSort":0,"description":"Athyrium x 'Ghost'","catalog":"From the garden of Virginia's Nancy Swell comes this stunning Lady Fern with silver-white fronds and a decidedly upright habit. Leaves age to light green with new fronds appearing throughout the season. Upright with a beautiful formal appearance that real","flowerColor":"","foliageColor":"Silver-Green","flowerTime":"","height":"18-24\"","spread":"12-18\"","type":"Default","exposure":"Shade to Part Shade","commonName":"Ghost Fern","category":"Fern","comment1":"","zone":"4-8","fallColor":"","shapeForm":"","soil":"Moist, Well-Drained","growth":"Medium","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"PG","cf2":"","cf3":"","templateId":1,"availQty":640,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":573,"prodId":"ATHYRXXXGHOS32","itemKey":69,"size":"32","sizeSort":0,"description":"Athyrium x 'Ghost'","catalog":"From the garden of Virginia's Nancy Swell comes this stunning Lady Fern with silver-white fronds and a decidedly upright habit. Leaves age to light green with new fronds appearing throughout the season. Upright with a beautiful formal appearance that real","flowerColor":"","foliageColor":"Silver-Green","flowerTime":"","height":"18-24\"","spread":"12-18\"","type":"Default","exposure":"Shade to Part Shade","commonName":"Ghost Fern","category":"Fern","comment1":"","zone":"4-8","fallColor":"","shapeForm":"","soil":"Moist, Well-Drained","growth":"Medium","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"PG","cf2":"","cf3":"","templateId":2,"availQty":640,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":99,"prodId":"CAREXMORSILV32","itemKey":109,"size":"32","sizeSort":0,"description":"Carex 'Silver Sceptre'","catalog":"A beautiful selection of this sedge, this one has narrow (1/4\") leaves with white margins, giving it a very fine texture overall. Rhizomatous, forming thick silvery clumps. A bright addition to the shade palette!","flowerColor":"","foliageColor":"","flowerTime":"","height":"10-12\"","spread":"1'","type":"Default","exposure":"Part Shade Shade","commonName":"","category":"Grass","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"DV","cf2":"","cf3":"","templateId":1,"availQty":230,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":595,"prodId":"CAREXMORSILV32","itemKey":109,"size":"32","sizeSort":0,"description":"Carex 'Silver Sceptre'","catalog":"A beautiful selection of this sedge, this one has narrow (1/4\") leaves with white margins, giving it a very fine texture overall. Rhizomatous, forming thick silvery clumps. A bright addition to the shade palette!","flowerColor":"","foliageColor":"","flowerTime":"","height":"10-12\"","spread":"1'","type":"Default","exposure":"Part Shade Shade","commonName":"","category":"Grass","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"DV","cf2":"","cf3":"","templateId":2,"availQty":230,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":761,"prodId":"1235692","itemKey":1326,"size":"32","sizeSort":0,"description":"Carex cherokeensis","catalog":"This sedge has rich green leaves that form a tidy arching clump and are evergreen in milder climates. Flowering is in late spring and has tan inflorescenses on top of wispy spikes above the foliage - they are small but add a bit of interest and texture.  This sedge prefers moist conditions and part shade but will tolerate average soils and a bit more sun.  North American native.","flowerColor":"Tan","foliageColor":"Green","flowerTime":"Late Spring","height":"18-24\"","spread":"24\"-48\"","type":"Default","exposure":"Part Shade  to Part Sun","commonName":"Cherokee sedge","category":"Grass","comment1":"","zone":"6-9","fallColor":"","shapeForm":"Arching","soil":"Average to Moist","growth":"","fruit":"","habit":"rhizomonous","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"TC","cf2":"","cf3":"","templateId":2,"availQty":536,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":457,"prodId":"1235692","itemKey":1326,"size":"32","sizeSort":0,"description":"Carex cherokeensis","catalog":"This sedge has rich green leaves that form a tidy arching clump and are evergreen in milder climates. Flowering is in late spring and has tan inflorescenses on top of wispy spikes above the foliage - they are small but add a bit of interest and texture.  This sedge prefers moist conditions and part shade but will tolerate average soils and a bit more sun.  North American native.","flowerColor":"Tan","foliageColor":"Green","flowerTime":"Late Spring","height":"18-24\"","spread":"24\"-48\"","type":"Default","exposure":"Part Shade  to Part Sun","commonName":"Cherokee sedge","category":"Grass","comment1":"","zone":"6-9","fallColor":"","shapeForm":"Arching","soil":"Average to Moist","growth":"","fruit":"","habit":"rhizomonous","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"TC","cf2":"","cf3":"","templateId":1,"availQty":536,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":100,"prodId":"1235973","itemKey":110,"size":"32","sizeSort":0,"description":"Carex dol. 'Kaga-nishiki' Gold Fountains","catalog":"This shade-loving sister of C. 'Evergold' is bright gold with attractive narrow (1/4\") foliage almost all year. Striking, long-lived and reliable. Works well in average garden conditions and in containers. prefers average to dry, fertile,well-drained soil","flowerColor":"","foliageColor":"","flowerTime":"","height":"10-12\"","spread":"10-12\", 20-30 cm","type":"Default","exposure":"","commonName":"","category":"Grass","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"DV","cf2":"","cf3":"","templateId":1,"availQty":226,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":596,"prodId":"1235973","itemKey":110,"size":"32","sizeSort":0,"description":"Carex dol. 'Kaga-nishiki' Gold Fountains","catalog":"This shade-loving sister of C. 'Evergold' is bright gold with attractive narrow (1/4\") foliage almost all year. Striking, long-lived and reliable. Works well in average garden conditions and in containers. prefers average to dry, fertile,well-drained soil","flowerColor":"","foliageColor":"","flowerTime":"","height":"10-12\"","spread":"10-12\", 20-30 cm","type":"Default","exposure":"","commonName":"","category":"Grass","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"DV","cf2":"","cf3":"","templateId":2,"availQty":226,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":89,"prodId":"CAREXFLABLUE32","itemKey":98,"size":"32","sizeSort":0,"description":"Carex flacca 'Blue Zinger'","catalog":"This Emerald Coast intrduction is indeed an improvement over the species. It is much more light blue than what we'd previously grown. Excellent and versatile shade groundcover for dry or moist spots. Cool season, evergreen in warm climates, more clump for","flowerColor":"","foliageColor":"","flowerTime":"","height":"1'","spread":"1-2'","type":"Default","exposure":"Part Shade Shade","commonName":"Glaucous Sedge","category":"Grass","comment1":"","zone":"5-8","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"DV","cf2":"","cf3":"","templateId":1,"availQty":922,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":585,"prodId":"CAREXFLABLUE32","itemKey":98,"size":"32","sizeSort":0,"description":"Carex flacca 'Blue Zinger'","catalog":"This Emerald Coast intrduction is indeed an improvement over the species. It is much more light blue than what we'd previously grown. Excellent and versatile shade groundcover for dry or moist spots. Cool season, evergreen in warm climates, more clump for","flowerColor":"","foliageColor":"","flowerTime":"","height":"1'","spread":"1-2'","type":"Default","exposure":"Part Shade Shade","commonName":"Glaucous Sedge","category":"Grass","comment1":"","zone":"5-8","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"DV","cf2":"","cf3":"","templateId":2,"availQty":922,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":103,"prodId":"CAREXMORICED32","itemKey":115,"size":"32","sizeSort":0,"description":"Carex morrowii 'Ice Dance'","catalog":"A bright groundcover for a shady spot, Ice Dance has long shiny leaves trimmed in bright white. It spreads slowly to fill in and make a tidy cover that discourages weeds. Deer and disease resistant, it is long-lasting and easy to grow!  SEMI-EVERGREEN.","flowerColor":"","foliageColor":"","flowerTime":"","height":"15-18\"","spread":"1-2'","type":"Default","exposure":"Part Shade Shade","commonName":"","category":"Grass","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"DV","cf2":"","cf3":"","templateId":1,"availQty":583,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":599,"prodId":"CAREXMORICED32","itemKey":115,"size":"32","sizeSort":0,"description":"Carex morrowii 'Ice Dance'","catalog":"A bright groundcover for a shady spot, Ice Dance has long shiny leaves trimmed in bright white. It spreads slowly to fill in and make a tidy cover that discourages weeds. Deer and disease resistant, it is long-lasting and easy to grow!  SEMI-EVERGREEN.","flowerColor":"","foliageColor":"","flowerTime":"","height":"15-18\"","spread":"1-2'","type":"Default","exposure":"Part Shade Shade","commonName":"","category":"Grass","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"DV","cf2":"","cf3":"","templateId":2,"availQty":1250,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":600,"prodId":"CAREXOSHEVER32","itemKey":116,"size":"32","sizeSort":0,"description":"Carex oshimensis 'Evergold'","catalog":"Fountains of narrow leaves with broad cream stripes adorn this clump-forming, shade-loving grass. Evergold is lovely spilling over into a path or as an architectural feature in a container or window box. Deer and disease resistant, it is long-lasting and ","flowerColor":"","foliageColor":"","flowerTime":"","height":"1'","spread":"1'","type":"Default","exposure":"Part Shade Shade","commonName":"","category":"Grass","comment1":"","zone":"5-8","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"DV","cf2":"","cf3":"","templateId":2,"availQty":418,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":104,"prodId":"CAREXOSHEVER32","itemKey":116,"size":"32","sizeSort":0,"description":"Carex oshimensis 'Evergold'","catalog":"Fountains of narrow leaves with broad cream stripes adorn this clump-forming, shade-loving grass. Evergold is lovely spilling over into a path or as an architectural feature in a container or window box. Deer and disease resistant, it is long-lasting and ","flowerColor":"","foliageColor":"","flowerTime":"","height":"1'","spread":"1'","type":"Default","exposure":"Part Shade Shade","commonName":"","category":"Grass","comment1":"","zone":"5-8","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"DV","cf2":"","cf3":"","templateId":1,"availQty":418,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":107,"prodId":"CARYOXXXDARK32","itemKey":119,"size":"32","sizeSort":0,"description":"Caryopteris x 'Dark Knight'","catalog":"Blue Mist Shrub is aptly named as its gray-green foliage is shrouded in a cloud of blue from mid to late summer. It is a well-behaved garden plant that is attractive to butterflies. Dark Knight has deep purple blue flowers closely spaced on long stems.","flowerColor":"Blue","foliageColor":"","flowerTime":"Late Summer","height":"24-30\"","spread":"","type":"Default","exposure":"Sun","commonName":"Blue Mist","category":"Woody","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"UR","cf2":"","cf3":"","templateId":1,"availQty":73,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":603,"prodId":"CARYOXXXDARK32","itemKey":119,"size":"32","sizeSort":0,"description":"Caryopteris x 'Dark Knight'","catalog":"Blue Mist Shrub is aptly named as its gray-green foliage is shrouded in a cloud of blue from mid to late summer. It is a well-behaved garden plant that is attractive to butterflies. Dark Knight has deep purple blue flowers closely spaced on long stems.","flowerColor":"Blue","foliageColor":"","flowerTime":"Late Summer","height":"24-30\"","spread":"","type":"Default","exposure":"Sun","commonName":"Blue Mist","category":"Woody","comment1":"","zone":"5-9","fallColor":"","shapeForm":"","soil":"","growth":"","fruit":"","habit":"","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"UR","cf2":"","cf3":"","templateId":2,"availQty":73,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":775,"prodId":"1236946","itemKey":1589,"size":"32","sizeSort":0,"description":"Caryopteris x clandonensis Blue Empire PPAF","catalog":"big, blue, beautiful!  Blue flowers in late summer larger than other Caryopteris flowers.  Bumblebees love this one at NCN.","flowerColor":"Blue","foliageColor":"Silver-Green","flowerTime":"Late Summer","height":"30-40\"","spread":"24-36\"","type":"Default","exposure":"Full Sun","commonName":"","category":"Woody","comment1":"","zone":"5-9","fallColor":"","shapeForm":"Upright","soil":"","growth":"Fast","fruit":"","habit":"clump","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"UR","cf2":"","cf3":"Concepts Plants BV","templateId":2,"availQty":157,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":483,"prodId":"1236946","itemKey":1589,"size":"32","sizeSort":0,"description":"Caryopteris x clandonensis Blue Empire PPAF","catalog":"big, blue, beautiful!  Blue flowers in late summer larger than other Caryopteris flowers.  Bumblebees love this one at NCN.","flowerColor":"Blue","foliageColor":"Silver-Green","flowerTime":"Late Summer","height":"30-40\"","spread":"24-36\"","type":"Default","exposure":"Full Sun","commonName":"","category":"Woody","comment1":"","zone":"5-9","fallColor":"","shapeForm":"Upright","soil":"","growth":"Fast","fruit":"","habit":"clump","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"UR","cf2":"","cf3":"Concepts Plants BV","templateId":1,"availQty":157,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":488,"prodId":"1237130","itemKey":1615,"size":"32","sizeSort":0,"description":"Coreopsis tripteris ‘Gold Standard’","catalog":"native superior selection made by Mt. Cuba Center in Alabama, and one of the top performers in their Coreopsis trials.  A bit shorter than the straight species, and less floppy.  Impressive over 2 month long floral display July - Sept.   C. tripteris tends to have great resistance to mildews.","flowerColor":"Golden Yellow","foliageColor":"Green","flowerTime":"Late Summer to Fall","height":"5-6'","spread":"","type":"Default","exposure":"Full Sun","commonName":"","category":"Perennial","comment1":"Sales","zone":"","fallColor":"","shapeForm":"Upright","soil":"","growth":"","fruit":"","habit":"rhizomonous","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"UR","cf2":"","cf3":"","templateId":1,"availQty":32,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},{"pk":778,"prodId":"1237130","itemKey":1615,"size":"32","sizeSort":0,"description":"Coreopsis tripteris ‘Gold Standard’","catalog":"native superior selection made by Mt. Cuba Center in Alabama, and one of the top performers in their Coreopsis trials.  A bit shorter than the straight species, and less floppy.  Impressive over 2 month long floral display July - Sept.   C. tripteris tends to have great resistance to mildews.","flowerColor":"Golden Yellow","foliageColor":"Green","flowerTime":"Late Summer to Fall","height":"5-6'","spread":"","type":"Default","exposure":"Full Sun","commonName":"","category":"Perennial","comment1":"Sales","zone":"","fallColor":"","shapeForm":"Upright","soil":"","growth":"","fruit":"","habit":"rhizomonous","flower":"","barkColor":"","genus":"","species":"","cultivar":"","custom1":"","custom2":"","cf1":"UR","cf2":"","cf3":"","templateId":2,"availQty":32,"qtyShip":1,"comments":"","price":0,"grade":"","imgs":[]},
[TRUNCATED DUE TO LENGTH]
```

**Catalog Field Descriptions**

Field | Description
--- | ---
pk | Unique numerical identifier for product
prodId | Product Code
itemKey | numerical id linking products that share catalog characteristics,
size | Size of this product,
sizeSort | index used for sorting this size,
description | product desciption,
catalog | catalog description,
flowerColor | flower color
foliageColor | foliage color,
flowerTime | flower time,
height | height
spread | spread
type | product type,
exposure | exposure,
commonName | common name,
category | product category,
comment1 | free entry comment1
zone | zone,
fallColor | fall color,
shapeForm | shape form,
soil | soil type,
growth | growth,
fruit | fruit,
habit | habit,
flower | flower,
barkColor | bark color,
genus | genus,
species | species,
cultivar | cultivar,
custom1 | custom field 1,
custom2 | custom field 2,
cf1 | cf1-Customer Defined,
cf2 | cf2-Customer Defined,
cf3 | cf3-Customer Definied,
templateId | availability template #,
availQty | available quantity,
qtyShip | minimum shippable qty,
comments | comments,
price | sales price,
grade | grade,
imgs | If set, this may contain an array of catalog image urls

#### Check Pricing ####

* Url: http://apps.sbiteam.com/orders-qa/api/100335/broker/prices?shipid=13305&shipdate=10/10/2019&qty=1&prodids=34719,31344
  * Parameters:
  * shipId = This is a single shipId corresponding to cntID returned by the /customers endpoint
  * shipdate = The ship date you want to check pricing on
  * qty = The quantity to check for pricing on 
  * prodids = A one-to-many list of prodIds for products you want to check pricing on. If multiple products then separate with a comma. 
* Header requires Authorization 

**Example Request**

```
GET /orders-qa/api/100335/broker/prices?shipid=13305&shipdate=10/10/2019&qty=1&prodids=34719,31344 HTTP/1.1
Authorization: Bearer [hidden]
User-Agent: PostmanRuntime/7.16.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 02698f8c-fd86-43d0-b2c0-cdd8ac6410e4
Host: apps.sbiteam.com
Accept-Encoding: gzip, deflate
Connection: keep-alive
```

**Example Response**

```
HTTP/1.1 200 OK
Server: nginx/1.10.3 (Ubuntu)
Date: Mon, 23 Sep 2019 22:20:20 GMT
Content-Type: text/html; charset=utf-8
Connection: keep-alive
X-Powered-By: Express
App-Version: 1.5.34
ETag: W/"45-5fPtCv1fNn3cH6tXt5QvjSRDZQI"
Vary: Accept-Encoding
Content-Length: 69

[{"prodId":"34719","price":168.26},{"prodId":"31344","price":221.19}]
```

#### Create Order ####

* Url: http://apps.sbiteam.com/orders-qa/api/100335/broker/orders
* POST to this url
* Header requires Authorization 

Body Json Example Order:

```
{
	"root": 
	{
		"orderHeader": 
		{
				"customerPK":13305,
				"customerPO":"test",
				"notes":"",
				"salesperson":"",
				"shipDate":"2019-10-16",
				"shipVia":"",
				"template":7,
				"shipToCompany":"Test Company",
				"shipToAddress1":"Test Address1",
				"shipToCity":"Test City",
				"shipToState":"OR",
				"shipToZip":"90210"
		},
		"orderDetail":
			[{
				"product":"152654",
				"qty":1,
				"retailSalesPrice":62.723,
				"retailpriceoverride":0
			}]
	}
}
```

** OrderHeader **

Field | Description
--- | ---
customerPK | ship-to (customerPK) from customer endpoint
customerPO | PO Number
notes | notes
salesperson | salesperson name
shipDate | Order's Ship Date
shipVia | Leave blank unless using a specific ship via provided by the grower
template | One of the available templates from the customer endpoint
shipToCompany | Ship To Company
shipToAddress1 | Ship To Address1
shipToCity | Ship To City
shipToState | Ship To State
shipToZip | Ship To Zip

** Order Detail Array **

Field | Description
--- | ---
product | prodId from catalog
qty | Qty to order
retailSalesPrice | Retail sales price
retailSalesPriceOverride | Flag this as a manual price

** Example Request **
```
POST /orders-qa/api/100335/broker/orders HTTP/1.1
Content-Type: application/json
Authorization: Bearer [hidden]
User-Agent: PostmanRuntime/7.16.3
Accept: */*
Cache-Control: no-cache
Postman-Token: 92db1019-a51e-4dbd-9d2d-0ddbf3385fb8
Host: apps.sbiteam.com
Accept-Encoding: gzip, deflate
Content-Length: 475
Connection: keep-alive
```

** Example Response **
```
HTTP/1.1 200 OK
Server: nginx/1.10.3 (Ubuntu)
Date: Mon, 23 Sep 2019 23:00:36 GMT
Content-Type: text/html; charset=utf-8
Connection: keep-alive
X-Powered-By: Express
App-Version: 1.5.34
ETag: W/"b1-Cqfix7Y9IHqXOKHrFiEC6Dx9VXE"
Vary: Accept-Encoding
Content-Length: 177

{"message":"Your order was submitted successfully.  Your order number is xxxWEB-201621xxx.  An order confirmation email will be sent to you shortly.","ordernumber":"WEB-201621"}
```

#### Query Orders ####

* Url: http://apps.sbiteam.com/orders-qa/api/100335/broker/orders
* GET to this url
* Header requires Authorization 
* This will return orders associated with your platform user

** Example Request **

```
GET /orders-qa/api/100335/broker/orders HTTP/1.1
Content-Type: application/json
Authorization: Bearer [hidden]
User-Agent: PostmanRuntime/7.16.3
Accept: */*
Cache-Control: no-cache
Postman-Token: be2650c4-596d-4363-8895-35742a515a1c
Host: apps.sbiteam.com
Accept-Encoding: gzip, deflate
Content-Length: 475
Connection: keep-alive
```

** Example Response **

```
HTTP/1.1 200 OK
Server: nginx/1.10.3 (Ubuntu)
Date: Mon, 23 Sep 2019 23:10:58 GMT
Content-Type: application/json; charset=utf-8
Connection: keep-alive
X-Powered-By: Express
App-Version: 1.5.34
ETag: W/"43e-eOO2h3k6n+OTkeUztDLQjejqhrk"
Vary: Accept-Encoding
Content-Length: 1086

[{"statusId":1,"pk":120219,"dateCreated":"2019-09-23T16:00:36.133Z","shipToId":13305,"orderNumber":"WEB-201621","ordId":546586,"orderTotal":0,"saved":true},{"statusId":1,"pk":120218,"dateCreated":"2019-09-23T15:49:39.253Z","shipToId":13305,"orderNumber":"WEB-201620","ordId":546585,"orderTotal":0,"saved":true},{"statusId":1,"pk":120217,"dateCreated":"2019-09-23T15:43:59.420Z","shipToId":13305,"orderNumber":"WEB-201619","ordId":546584,"orderTotal":0,"saved":true},{"statusId":1,"pk":120216,"dateCreated":"2019-09-23T15:43:53.093Z","shipToId":13305,"orderNumber":"WEB-201618","ordId":546583,"orderTotal":0,"saved":true},{"statusId":1,"pk":120215,"dateCreated":"2019-09-23T15:43:44.470Z","shipToId":13305,"orderNumber":"WEB-201617","ordId":546582,"orderTotal":0,"saved":true},{"statusId":1,"pk":120214,"dateCreated":"2019-09-23T15:43:30.166Z","shipToId":13305,"orderNumber":"WEB-201616","ordId":546581,"orderTotal":0,"saved":true},{"statusId":1,"pk":120213,"dateCreated":"2019-09-23T15:35:40.890Z","shipToId":13305,"orderNumber":"WEB-201615","ordId":546580,"orderTotal":0,"saved":true}]
```